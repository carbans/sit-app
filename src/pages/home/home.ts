import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public http: Http) {
  }

  descargarCampos(){
    this.http.get('http://127.0.0.1:8000/api/campos/?format=json')
  }

}
